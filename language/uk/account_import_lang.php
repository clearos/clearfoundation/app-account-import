<?php

$lang['account_import_app_description'] = 'Додаток «Імпорт облікового запису» виконує масовий імпорт облікових записів за допомогою файлу у форматі значень, розділених комами (CSV).';
$lang['account_import_app_name'] = 'Імпорт облікового запису';
$lang['account_import_clear'] = 'Чистити';
$lang['account_import_csv_file'] = 'Файл CSV';
$lang['account_import_csv_file_not_found'] = 'Файл CSV не знайдено.';
$lang['account_import_download_csv_template'] = 'Завантажити шаблон CSV';
$lang['account_import_import_already_in_progress'] = 'Імпорт уже триває.';
$lang['account_import_import_complete'] = 'Імпорт завершено.';
$lang['account_import_import_log'] = 'Журнал імпорту';
$lang['account_import_import_users'] = 'Імпорт користувачів';
$lang['account_import_importing_user'] = 'Імпорт користувача';
$lang['account_import_no_data_found'] = 'Даних не знайдено.';
$lang['account_import_no_entries'] = 'Записів для імпорту не знайдено.';
$lang['account_import_number_of_records'] = 'Кількість записів';
$lang['account_import_start_import'] = 'Почніть імпорт';
$lang['account_import_tooltip_csv'] = 'Переконайтеся, що ваш файл CSV відформатовано належним чином – він має містити значення, узяті в лапки та розділені комою ("value1", "value2" тощо).';
$lang['account_import_unable_to_determine_running_state'] = 'Неможливо визначити, чи виконується імпорт.';
$lang['account_import_upload_csv_file'] = 'Завантажте файл CSV';
